﻿using FluentAssertions;
using HaveBox;
using HaveBoxStates;
using HaveBoxStates.ContainerAdaptors;
using Xunit;

namespace HaveBoxStatesTests
{
    public class StateTests
    {
        [Fact]
        public void Given_States_When_Starting_The_State_Machine_Then_The_Right_State_Is_Started()
        {
            var container = new Container();
            container.Configure(config => {
                config.For<FirstState>().Use<FirstState>();
                config.For<SecondState>().Use<SecondState>();
            });
            var stateMachine = new StateMachine(new HaveBoxAdaptor(container));
            var dto = new HelloWorldDto { StopAtNextState = true };

            stateMachine.StartStateMachinesAtState<FirstState>(dto);

            stateMachine.CurrentState.Should().BeOfType<Stop>();
        }

        [Fact]
        public void Given_A_State_When_Starting_State_Machine_Then_The_State_Is_Runned()
        {
            var container = new Container();
            container.Configure(config =>
            {
                config.For<FirstState>().Use<FirstState>();
            });
            var stateMachine = new StateMachine(new HaveBoxAdaptor(container));
            var dto = new HelloWorldDto { StopAtNextState = true };

            stateMachine.StartStateMachinesAtState<FirstState>(dto);

            dto.Text.Should().Be("Hello World");
        }

        [Fact]
        public void Given_States_When_Starting_Machine_The_There_Is_Switched_Through_States()
        {
            var container = new Container();
            container.Configure(config =>
            {
                config.For<FirstState>().Use<FirstState>();
                config.For<SecondState>().Use<SecondState>();
                config.For<ThirdState>().Use<ThirdState>();
            });
            var stateMachine = new StateMachine(new HaveBoxAdaptor(container));
            var dto = new HelloWorldDto();

            stateMachine.StartStateMachinesAtState<FirstState>(dto);

            dto.Text.Should().Be("Hello World");
        }

        [Fact]
        public void Given_A_Stats_Without_Dto_When_Starting_The_State_Machine_Then_The_Right_State_Is_Started()
        {
            var container = new Container();
            container.Configure(config =>
            {
                config.For<FourthState>().Use<FourthState>();
            });
            var stateMachine = new StateMachine(new HaveBoxAdaptor(container));

            stateMachine.StartStateMachinesAtState<FourthState>();

            stateMachine.CurrentState.Should().BeOfType<Stop>();
        }

        public class HelloWorldDto
        {
            public string Text { get; set; }
            public bool StopAtNextState { get; set; }
        }

        public class FirstState : IState
        {
            public void ExecuteState(IStateMachine statemachine, HelloWorldDto dto)
            {
                if (dto.StopAtNextState)
                {
                    dto.Text = "Hello World";
                    statemachine.SetNextStateTo<Stop>();
                }
                else
                {
                    statemachine.SetNextStateTo<SecondState>(dto);
                }
            }
        }

        public class SecondState : IState
        {
            public void ExecuteState(IStateMachine statemachine, HelloWorldDto dto)
            {
                statemachine.SetNextStateTo<ThirdState>(dto);
            }
        }

        public class ThirdState : IState
        {
            public void ExecuteState(IStateMachine statemachine, HelloWorldDto dto)
            {
                dto.Text = "Hello World";
                statemachine.SetNextStateTo<Stop>();
            }
        }

        public class FourthState : IState
        {
            public void ExecuteState(IStateMachine statemachine)
            {
                statemachine.SetNextStateTo<Stop>();
            }
        }        
    }
}
