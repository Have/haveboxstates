﻿namespace HaveBoxStates
{
    public interface IStateMachine
    {
        void StartStateMachinesAtState<STATE>(object dto);
        void StartStateMachinesAtState<STATE>();

        void SetNextStateTo<STATE>(object dto);
        void SetNextStateTo<STATE>();
    }
}
