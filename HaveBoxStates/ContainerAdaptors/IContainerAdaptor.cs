﻿
namespace HaveBoxStates.ContainerAdaptors
{
    public interface IContainerAdaptor
    {
        T GetInstance<T>();
        void AddState<T>();
    }
}
