﻿using HaveBox;

namespace HaveBoxStates.ContainerAdaptors
{
    public class HaveBoxAdaptor : IContainerAdaptor
    {
        private IContainer _container;

        public HaveBoxAdaptor(IContainer container)
        {
            _container = container;
        }

        public T GetInstance<T>()
        {
            return _container.GetInstance<T>();
        }

        public void AddState<T>()
        {
            _container.Configure(config => config.For<T>().Use<T>());
        }
    }
}
