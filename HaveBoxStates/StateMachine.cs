﻿using HaveBoxStates.ContainerAdaptors;
using System;

namespace HaveBoxStates
{
    public class StateMachine : IStateMachine
    {
        private IContainerAdaptor _containerAdaptor;
        private IState _nextState;
        private object _nextDto;
        private object _currentDto;
        private bool _useExecuteStateWithStateMachineArgOnly;

        public IState CurrentState { get; private set; }

        public StateMachine(IContainerAdaptor containerAdaptor)
        {
            _containerAdaptor = containerAdaptor;
            _containerAdaptor.AddState<Stop>();

            _useExecuteStateWithStateMachineArgOnly = false;
        }

        public void StartStateMachinesAtState<STATE>(object dto)
        {
            SetNextStateTo<STATE>(dto);

            for (; ; )
            {
                if (_nextState == null)
                {
                    continue;
                }

                CurrentState = _nextState;
                _currentDto = _nextDto;

                if (CurrentState.GetType() == typeof(Stop))
                {
                    break;
                }

                _nextState = null;
                _nextDto = null;

                var method = CurrentState.GetType().GetMethod("ExecuteState");

                if (_useExecuteStateWithStateMachineArgOnly)
                {
                    _useExecuteStateWithStateMachineArgOnly = false;
                    method.Invoke(CurrentState, new[] { this });
                }
                else
                {
                    method.Invoke(CurrentState, new[] { this, Convert.ChangeType(_currentDto, method.GetParameters()[1].ParameterType) });
                }
            }
        }

        public void SetNextStateTo<STATE>(object dto)
        {
            _nextState = _containerAdaptor.GetInstance<STATE>() as IState;
            _nextDto = dto;
        }

        public void StartStateMachinesAtState<STATE>()
        {
            _useExecuteStateWithStateMachineArgOnly = true;
            StartStateMachinesAtState<STATE>(null);
        }

        public void SetNextStateTo<STATE>()
        {
            _useExecuteStateWithStateMachineArgOnly = true;
            SetNextStateTo<STATE>(null);
        }
    }
}
