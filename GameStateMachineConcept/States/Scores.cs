﻿using GameStateMachineConcept.Dtos;
using HaveBoxStates;

namespace GameStateMachineConcept.States
{
    public class Scores : IState
    {
        public void ExecuteState(IStateMachine statemachine, GameContext GameContext)
        {
            GameContext.PlayerContext.Points += GameContext.GainedPoints;

            if (GameContext.PlayerContext.Lives == 0)
            {
                statemachine.SetNextStateTo<Stop>();
            }
            else
            {
                statemachine.SetNextStateTo<PlayGame>(GameContext.PlayerContext);
            }
        }
    }
}
