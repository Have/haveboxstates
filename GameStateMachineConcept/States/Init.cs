﻿using GameStateMachineConcept.Dtos;
using HaveBoxStates;

namespace GameStateMachineConcept.States
{
    public class Init : IState
    {
        public void ExecuteState(IStateMachine statemachine)
        {
            statemachine.SetNextStateTo<PlayGame>(new PlayerContext{ Points = 0, Lives = 3, });
        }
    }
}
