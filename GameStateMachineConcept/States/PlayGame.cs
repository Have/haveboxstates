﻿using GameStateMachineConcept.Dtos;
using GameStateMachineConcept.GameEngine;
using HaveBoxStates;

namespace GameStateMachineConcept.States
{
    public class PlayGame : IState
    {
        private IGameEngine _gameEngine;

        public PlayGame(IGameEngine gameEngine)
        {
            _gameEngine = gameEngine;
        }

        public void ExecuteState(IStateMachine statemachine, PlayerContext playerContext)
        {
            var gameContext = new GameContext { PlayerContext = playerContext, GainedPoints = 0, };

            _gameEngine.Play(gameContext);

            statemachine.SetNextStateTo<Scores>(gameContext);
        }
    }
}
