﻿using GameStateMachineConcept.GameEngine;
using GameStateMachineConcept.States;
using HaveBox;
using HaveBoxStates;
using HaveBoxStates.ContainerAdaptors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStateMachineConcept
{
    public class Program
    {
        static void Main(string[] args)
        {
            var container = new Container();
            container.Configure(config =>
            {
                config.For<IGameEngine>().Use<GameEngine.GameEngine>();
                config.For<Init>().Use<Init>();
                config.For<PlayGame>().Use<PlayGame>();
                config.For<Scores>().Use<Scores>();
            });

            var stateMachine = new StateMachine(new HaveBoxAdaptor(container));
            stateMachine.StartStateMachinesAtState<Init>();
        }
    }
}
