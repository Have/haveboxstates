﻿
namespace GameStateMachineConcept.Dtos
{
    public class GameContext
    {
        public PlayerContext PlayerContext { get; set; }
        public int GainedPoints { get; set; }
    }
}
