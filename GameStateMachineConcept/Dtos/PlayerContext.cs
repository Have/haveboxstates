﻿
namespace GameStateMachineConcept.Dtos
{
    public class PlayerContext
    {
        public int Points { get; set; }
        public int Lives { get; set; }
    }
}
