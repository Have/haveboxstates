﻿namespace GameStateMachineConcept.GameEngine
{
    public interface IGameEngine
    {
        void Play(GameStateMachineConcept.Dtos.GameContext gameContext);
    }
}
