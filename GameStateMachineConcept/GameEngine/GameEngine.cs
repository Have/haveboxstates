﻿using GameStateMachineConcept.Dtos;

namespace GameStateMachineConcept.GameEngine
{
    public class GameEngine : GameStateMachineConcept.GameEngine.IGameEngine
    {
        public void Play(GameContext gameContext)
        {
            gameContext.PlayerContext.Lives--;
            gameContext.GainedPoints = 100;
        }
    }
}
